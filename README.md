### Host in heroku:

https://moviesmovies.herokuapp.com/


To access the entry point for views:

`/choices
`

Here, you will see UI operations for movies and persons.

**API**:

`/api/v1/persons`

##### Write operations

`{
    "alias": "tim"
}
`

***

`/api/v1/movies`

##### Write operations

`{
    "title": "The movie",
    "release_year": "1989-12-02",
    "casting_ids": [1, 2],
    "producer_ids": [1],
    "director_ids": [2, 1]
}`

_Take into account that the `_ids` objects already has to be exists._

# Things to improve and to consider:

1. Test views are not protected, I did not it due to time.
2. I use native django's test suite. I could use pytest but it's configuration could takes some time.
3. Address some pylint.
4. The deployment to Heroku is just for demonstration purpose, but is not intended to be a production environment. I did not want deal with DB hosts, that's why I decided to upload the `sqlite` file. Also, the `DEBUG` variable is set in `True` just for debug purpose at the time of review the code by the recruiter.
5. Tests for `movie` views are missing, I wrote tests for `person` views. I just wanted to write some simple tests. If we want to write tests for movies views just follow the same path.
6. Tests are simple. We could write more complex and robust tests. But again, due to deadline I just want to show the concept.
