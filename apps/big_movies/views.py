from django.views.generic import (
    CreateView,
    ListView,
    UpdateView,
    TemplateView,
    DeleteView,
)

from django.urls import reverse_lazy

from .models import Movie, Person


class IMDBView(TemplateView):
    template_name = "choices.html"


class CreatePersonView(CreateView):
    model = Person
    template_name = "create_person.html"
    fields = "__all__"
    success_url = reverse_lazy("all-persons")


class ListPersonsView(ListView):
    model = Person
    template_name = "list_persons.html"


class UpdatePersonView(UpdateView):
    model = Person
    template_name = "update_person.html"
    fields = "__all__"
    success_url = reverse_lazy("all-persons")


class DeletePersonView(DeleteView):
    model = Person
    success_url = reverse_lazy("all-persons")

    def get(self, request, *args, **kwargs):
        """Let's delete this without any confirmation page."""
        return self.post(request, *args, **kwargs)


class ListMoviesView(ListView):
    model = Movie
    template_name = "list_movies.html"


class CreateMovieView(CreateView):
    model = Movie
    fields = "__all__"
    template_name = "create_movie.html"
    success_url = reverse_lazy("all-movies")


class UpdateMovieView(UpdateView):
    model = Movie
    template_name = "update_movie.html"
    fields = "__all__"
    success_url = reverse_lazy("all-movies")


class DeleteMovieView(DeleteView):
    model = Movie
    success_url = reverse_lazy("all-movies")

    def get(self, request, *args, **kwargs):
        """Let's delete this without any confirmation page."""
        return self.post(request, *args, **kwargs)
