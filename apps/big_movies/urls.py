from django.urls import path

from .views import (
    CreatePersonView,
    ListPersonsView,
    UpdatePersonView,
    DeletePersonView,
    ListMoviesView,
    IMDBView,
    CreateMovieView,
    UpdateMovieView,
    DeleteMovieView,
)


urlpatterns = [
    path("choices/", IMDBView.as_view(), name="home"),
    path("movies/all/", ListMoviesView.as_view(), name="all-movies"),
    path("movies/create/", CreateMovieView.as_view(), name="create-movie"),
    path("movies/edit/<slug:pk>/", UpdateMovieView.as_view(), name="update-movie"),
    path("movie/delete/<slug:pk>/", DeleteMovieView.as_view(), name="delete-movie"),
    path("persons/all/", ListPersonsView.as_view(), name="all-persons"),
    path("person/create/", CreatePersonView.as_view(), name="create-person"),
    path("person/edit/<slug:pk>/", UpdatePersonView.as_view(), name="update-person"),
    path("person/delete/<slug:pk>/", DeletePersonView.as_view(), name="delete-person"),
]
