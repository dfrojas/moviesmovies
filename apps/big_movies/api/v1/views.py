from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from apps.big_movies.models import Movie, Person
from .serializers import MovieSerializer, PersonSerializer
from .permissions import ReadOnly


class MovieViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated | ReadOnly]
    http_method_names = ["get", "post", "patch", "put", "delete"]
    queryset = Movie.objects.all()
    model = Movie
    serializer_class = MovieSerializer


class PersonViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated | ReadOnly]
    http_method_names = ["get", "post", "patch", "put", "delete"]
    queryset = Person.objects.all()
    model = Person
    serializer_class = PersonSerializer
