from rest_framework import routers

from .views import MovieViewSet, PersonViewSet


router = routers.SimpleRouter()

router.register(r'movies', MovieViewSet)
router.register(r'persons', PersonViewSet)
