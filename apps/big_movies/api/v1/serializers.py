from rest_framework import serializers

from apps.big_movies.models import Movie, Person


class LightPersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = "__all__"


class MovieSerializer(serializers.ModelSerializer):
    """
    M2M fields are handles by _ids prefix in the write operations
    to avoid send the entire object.
    """
    roman_year_number = serializers.SerializerMethodField()
    casting = LightPersonSerializer(read_only=True, many=True)
    producers = LightPersonSerializer(read_only=True, many=True)
    directors = LightPersonSerializer(read_only=True, many=True)
    casting_ids = serializers.PrimaryKeyRelatedField(
        source="casting", queryset=Person.objects.all(), many=True, write_only=True
    )
    producer_ids = serializers.PrimaryKeyRelatedField(
        source="producers", queryset=Person.objects.all(), many=True, write_only=True
    )
    director_ids = serializers.PrimaryKeyRelatedField(
        source="directors", queryset=Person.objects.all(), many=True, write_only=True
    )

    def get_roman_year_number(self, obj):
        year = obj.release_year.year
        values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
        symbols = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
        roman_num = ""
        i = 0
        while year > 0:
            for _ in range(year // values[i]):
                roman_num += symbols[i]
                year -= values[i]
            i += 1
        return roman_num

    class Meta:
        model = Movie
        fields = "__all__"

    def to_representation(self, obj):
        ret = super(MovieSerializer, self).to_representation(obj)
        view = self.context.get("view")

        # We don't want these keys in the movies viewset due to
        # we already have them with as_directors, as_actor_or_actress and
        # as_producer.
        if view.basename == "person":
            del ret["casting"]
            del ret["directors"]
            del ret["producers"]

        return ret


class PersonSerializer(serializers.ModelSerializer):

    as_actor_or_actress = MovieSerializer(many=True, read_only=True)
    as_director = MovieSerializer(many=True, read_only=True)
    as_producer = MovieSerializer(many=True, read_only=True)

    class Meta:
        model = Person
        fields = "__all__"
