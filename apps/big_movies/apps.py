from django.apps import AppConfig


class BigMoviesConfig(AppConfig):
    name = 'big_movies'
