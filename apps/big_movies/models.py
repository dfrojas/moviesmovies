from django.db import models


class Person(models.Model):
    alias = models.CharField(max_length=100)

    def __str__(self):
        return self.alias


class Movie(models.Model):
    title = models.CharField(max_length=100)
    release_year = models.DateField()
    casting = models.ManyToManyField(Person, related_name="as_actor_or_actress")
    directors = models.ManyToManyField(Person, related_name="as_director")
    producers = models.ManyToManyField(Person, related_name="as_producer")

    def __str__(self):
        return self.title
