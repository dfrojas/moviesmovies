from django.test import TestCase
from django.urls import reverse

from apps.big_movies.models import Movie, Person


class PersonsViewTest(TestCase):

    def setUp(self):
        self.person = Person.objects.create(alias="AnyOtherPerson")

    def test_home_page(self):
        response = self.client.get(reverse("home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "choices.html")

    def test_create_person(self):
        response = self.client.get(reverse("create-person"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "create_person.html")
        self.assertEqual(len(Person.objects.all()), 1) # Before test the submit.

        person_data = {"alias": "Aperson"}

        response = self.client.post(reverse("create-person"), data=person_data)
        self.assertEqual(response.status_code, 302)  # We redirect to the persons list.
        self.assertEqual(len(Person.objects.all()), 2)  # After the submit.

    def test_update_person(self):
        response = self.client.get(reverse("update-person", kwargs={"pk": self.person.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "update_person.html")

        person_data = {"alias": "MockUser"}

        response = self.client.post(reverse("update-person", kwargs={"pk": self.person.id}), data=person_data)
        self.assertEqual(response.status_code, 302)  # We redirect to the persons list.
        self.assertEqual(Person.objects.get(pk=self.person.id).alias, "MockUser")

    def test_delete_person(self):
        response = self.client.get(reverse("delete-person", kwargs={"pk": self.person.id}))
        self.assertEqual(response.status_code, 302)  # We redirect to the persons list.
        self.assertFalse(Person.objects.filter(pk=self.person.id).exists())



