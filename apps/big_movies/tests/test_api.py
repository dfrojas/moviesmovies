import datetime

from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from apps.big_movies.models import Movie, Person


class MoviesAPITestCase(APITestCase):
    def setUp(self):

        self.user = User.objects.create_superuser(
            "admin", "admin@admin.com", "admin123"
        )
        self.token = Token.objects.create(user=self.user)
        self.client.force_login(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

        self.persons = [
            Person.objects.create(alias=f"whatever_{p}") for p in range(1, 6)
        ]

        persons = self.persons

        data = {"title": "The dawn of the code test", "release_year": "1989-12-02"}

        movie = Movie.objects.create(**data)
        movie.casting.add(persons[0], persons[1], persons[2])
        movie.directors.add(persons[3])
        movie.producers.add(persons[4])

        self.movie = (
            Movie.objects.all().values("id").first()
        )  # Just let's update the first one.

    def test_movies_get(self):
        request = self.client.get("/api/v1/movies/")
        expected_response = [
            {
                "id": 1,
                "roman_year_number": "MCMLXXXIX",
                "casting": [
                    {"id": 1, "alias": "whatever_1"},
                    {"id": 2, "alias": "whatever_2"},
                    {"id": 3, "alias": "whatever_3"},
                ],
                "producers": [{"id": 5, "alias": "whatever_5"}],
                "directors": [{"id": 4, "alias": "whatever_4"}],
                "title": "The dawn of the code test",
                "release_year": "1989-12-02",
            }
        ]

        self.assertEqual(request.json(), expected_response)
        self.assertEqual(request.status_code, 200)

    def test_movies_post(self):

        new_movie = {
            "title": "The dawn of the code test 2",
            "release_year": "2001-12-05",
            "casting_ids": [p.id for p in self.persons[0:2]],
            "producer_ids": [self.persons[3].id],
            "director_ids": [self.persons[4].id],
        }

        request = self.client.post("/api/v1/movies/", new_movie, format="json")

        expected_response = {
            "id": 2,
            "roman_year_number": "MMI",
            "casting": [
                {"id": 1, "alias": "whatever_1"},
                {"id": 2, "alias": "whatever_2"},
            ],
            "producers": [{"id": 4, "alias": "whatever_4"}],
            "directors": [{"id": 5, "alias": "whatever_5"}],
            "title": "The dawn of the code test 2",
            "release_year": "2001-12-05",
        }

        self.assertEqual(request.json(), expected_response)
        self.assertEqual(request.status_code, 201)
        # Truly a new movie was created in the DB by our API? Let's see.
        self.assertEqual(len(Movie.objects.all()), 2)

    def test_movies_update(self):
        movie_id = self.movie["id"]
        data_to_update = {"release_year": "1989-12-02"}
        request = self.client.patch(
            f"/api/v1/movies/{movie_id}/", data_to_update, format="json"
        )

        self.assertEqual(request.status_code, 200)
        # Truly the date was updated in the DB by our API? Let's see.
        self.assertEqual(
            Movie.objects.get(pk=movie_id).release_year, datetime.date(1989, 12, 2)
        )

    def test_movies_delete(self):
        movie_id = self.movie["id"]
        request = self.client.delete(f"/api/v1/movies/{movie_id}/")

        self.assertEqual(request.status_code, 204)
        # Truly the movie was deleted in the DB by our API? Let's see.
        self.assertEqual(len(Movie.objects.all()), 0)


class PersonsAPITestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            "admin", "admin@admin.com", "admin123"
        )
        self.token = Token.objects.create(user=self.user)
        self.client.force_login(user=self.user)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

        Person.objects.create(alias="ChristopherNolan")
        p2 = Person.objects.create(alias="Quentin")

        data = {"title": "Pulp Fiction", "release_year": "1991-05-21"}

        movie = Movie.objects.create(**data)
        movie.casting.add(p2)
        movie.directors.add(p2)

        self.person = (
            Person.objects.all().values("id").first()
        )  # Just let's update the first one.

    def test_persons_get(self):
        request = self.client.get("/api/v1/persons/")
        expected_response = [
            {
                "id": 1,
                "as_actor_or_actress": [],
                "as_director": [],
                "as_producer": [],
                "alias": "ChristopherNolan",
            },
            {
                "id": 2,
                "as_actor_or_actress": [
                    {
                        "id": 1,
                        "roman_year_number": "MCMXCI",
                        "title": "Pulp Fiction",
                        "release_year": "1991-05-21",
                    }
                ],
                "as_director": [
                    {
                        "id": 1,
                        "roman_year_number": "MCMXCI",
                        "title": "Pulp Fiction",
                        "release_year": "1991-05-21",
                    }
                ],
                "as_producer": [],
                "alias": "Quentin",
            },
        ]
        self.assertEqual(request.json(), expected_response)
        self.assertEqual(request.status_code, 200)

    def test_persons_post(self):
        new_person = {
            "alias": "TimBurton",
        }

        request = self.client.post("/api/v1/persons/", new_person, format="json")
        expected_response = {
            "id": 3,
            "as_actor_or_actress": [],
            "as_director": [],
            "as_producer": [],
            "alias": "TimBurton",
        }
        self.assertEqual(request.json(), expected_response)
        self.assertEqual(request.status_code, 201)

    def test_persons_update(self):
        person_id = self.person["id"]
        data_to_update = {"alias": "Chris"}
        request = self.client.patch(
            f"/api/v1/persons/{person_id}/", data_to_update, format="json"
        )

        expected_response = {
            "id": 1,
            "as_actor_or_actress": [],
            "as_director": [],
            "as_producer": [],
            "alias": "Chris",
        }

        self.assertEqual(request.json(), expected_response)
        self.assertEqual(request.status_code, 200)
        # Truly the date was updated in the DB by our API? Let's see.
        self.assertEqual(Person.objects.get(pk=person_id).alias, "Chris")

    def test_persons_delete(self):
        person_id = self.person["id"]
        request = self.client.delete(f"/api/v1/persons/{person_id}/")

        self.assertEqual(request.status_code, 204)
        # Truly the movie was deleted in the DB by our API? Let's see.
        self.assertEqual(len(Movie.objects.all()), 1)
